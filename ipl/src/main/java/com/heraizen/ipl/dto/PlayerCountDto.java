package com.heraizen.ipl.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlayerCountDto {
	private long count;
	private String role;

}
