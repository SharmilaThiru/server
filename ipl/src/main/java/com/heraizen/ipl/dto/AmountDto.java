package com.heraizen.ipl.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AmountDto {
	private double price;

}
