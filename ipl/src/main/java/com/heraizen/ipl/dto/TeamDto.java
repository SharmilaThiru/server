package com.heraizen.ipl.dto;

import java.util.List;

import com.heraizen.ipl.domain.Player;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class TeamDto {
	private String city;
	private String coach;
	private String home;
	private String name;
	private String label;
	private List<Player> players;
}
