package com.heraizen.ipl.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class PlayerDto {
	private String role;
	private String name;
	private double price;
	private String label;
}
