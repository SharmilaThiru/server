package com.heraizen.ipl.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleAmountDto {
	private double price;
	private String role;

}
