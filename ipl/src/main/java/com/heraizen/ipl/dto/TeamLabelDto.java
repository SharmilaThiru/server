package com.heraizen.ipl.dto;

import java.util.List;

import lombok.Data;
@Data
public class TeamLabelDto {
	private List<String> labels;

}
