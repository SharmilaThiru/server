package com.heraizen.ipl.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heraizen.ipl.domain.Team;
import com.heraizen.ipl.main.IplDriver;

public final class JsonReaderUtil {
	private JsonReaderUtil() {

	}

	private static List<Team> loadData(){
		return loadDataFromJson();
	}
//	 private static List<Team> loadData(FileType fileType){
//		 if(fileType==File)
//	 }
	
	public static List<Team> loadDataFromJson() {
		List<Team> list = new ArrayList<>();
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			list = objectMapper.readValue(JsonReaderUtil.class.getResourceAsStream("/team.json"),
					new TypeReference<List<Team>>() {
					});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;

	}

	public static List<Team> loadDataFromYaml() {
		Yaml yaml = new Yaml();
		Team[] list = yaml.loadAs(JsonReaderUtil.class.getResourceAsStream("/team.yaml"), Team[].class);
		return Arrays.asList(list);
	}

}
