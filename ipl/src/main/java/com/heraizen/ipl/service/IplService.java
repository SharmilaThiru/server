package com.heraizen.ipl.service;

import java.util.List;
import java.util.Map;

import com.heraizen.ipl.domain.Role;
import com.heraizen.ipl.dto.AmountDto;
import com.heraizen.ipl.dto.PlayerCountDto;
import com.heraizen.ipl.dto.PlayerDto;
import com.heraizen.ipl.dto.RoleAmountDto;
import com.heraizen.ipl.dto.TeamDto;
import com.heraizen.ipl.dto.TeamLabelsDto;

public interface IplService {
	public TeamLabelsDto getTeamLabels();//finished

	public List<PlayerDto> getPlayersByLabel(String label);//finished

	public List<PlayerCountDto> getRoleCountByLabel(String label);//finished

	public List<PlayerDto> getAllPlayers();//finished

	public List<PlayerDto> getPlayerByLabelAndRole(String label, Role role);

	public List<TeamDto> getTeamDetails();//finsished

	public List<AmountDto> getAmountSpentByTeam(String label);//finished

	public List<RoleAmountDto> getAmountSpentOnRoleByTeam(String label);//finished

	public Map<Role, List<PlayerDto>> getMaxPaidPlayerForEachRole();

}
