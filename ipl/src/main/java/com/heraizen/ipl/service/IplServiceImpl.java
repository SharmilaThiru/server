package com.heraizen.ipl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.heraizen.ipl.domain.Player;
import com.heraizen.ipl.domain.Role;
import com.heraizen.ipl.domain.Team;
import com.heraizen.ipl.dto.AmountDto;
import com.heraizen.ipl.dto.PlayerCountDto;
import com.heraizen.ipl.dto.PlayerDto;
import com.heraizen.ipl.dto.RoleAmountDto;
import com.heraizen.ipl.dto.TeamDto;
import com.heraizen.ipl.dto.TeamLabelsDto;
import com.heraizen.ipl.util.JsonReaderUtil;

public class IplServiceImpl implements IplService {
	private static transient IplServiceImpl obj = null;
	private List<Team> list;
	Team team;

	private IplServiceImpl() {
//		list = JsonReaderUtil.loadDataFromJson();
		list = JsonReaderUtil.loadDataFromYaml();

	}

	public static IplService getInstance() {
		if (obj == null) {
			synchronized (IplServiceImpl.class) {
				if (obj == null) {
					obj = new IplServiceImpl();
				}

			}
		}
		return obj;
	}

	@Override
	public TeamLabelsDto getTeamLabels() {
		if (list != null) {
			List<String> teamList = list.stream().map(Team::getLabel).collect(Collectors.toList());
			return TeamLabelsDto.builder().labels(teamList).build();

		}
		return null;

	}

	@Override
	public List<PlayerDto> getPlayersByLabel(String label) {
		List<PlayerDto> playerDetails = new ArrayList<>();
		list.stream().forEach(e -> {
			if (e.getLabel().equals(label)) {
				team = e;
			}
		});
		team.getPlayers().forEach(e -> {
			PlayerDto players = PlayerDto.builder().name(e.getName()).price(e.getPrice()).role(e.getRole())
					.label(team.getLabel()).build();
			playerDetails.add(players);
		});
		return playerDetails;
	}

	@Override
	public List<PlayerCountDto> getRoleCountByLabel(String label) {

		List<List<Player>> playerList = list.stream().filter(e -> e.getLabel().equals(label)).map(Team::getPlayers)
				.collect(Collectors.toList());

		List<PlayerCountDto> roleCount = new ArrayList<>();

		for (List<Player> list1 : playerList) {

			long allRounderCount = list1.stream().filter(e -> e.getRole().equals("All-Rounder")).map(Player::getRole)
					.count();
			long bowler = list1.stream().filter(e -> e.getRole().equals("Bowler")).map(Player::getRole).count();
			long wicketKeeper = list1.stream().filter(e -> e.getRole().equals("Wicket Keeper")).map(Player::getRole)
					.count();
			long batsMan = list1.stream().filter(e -> e.getRole().equals("Batsman")).map(Player::getRole).count();
			roleCount.add(PlayerCountDto.builder().role("All-Rounder").count(allRounderCount).build());
			roleCount.add(PlayerCountDto.builder().role("Bowler").count(bowler).build());
			roleCount.add(PlayerCountDto.builder().role("Wicket Keeper").count(wicketKeeper).build());
			roleCount.add(PlayerCountDto.builder().role("Batsman").count(batsMan).build());
		}

		return roleCount;

	}

	@Override
	public List<PlayerDto> getAllPlayers() {
		List<PlayerDto> playerDetails = new ArrayList<>();
		list.stream().forEach(e -> {
			e.getPlayers().forEach(p -> {
				PlayerDto allPlayers = PlayerDto.builder().label(e.getLabel()).name(p.getName()).price(p.getPrice())
						.role(p.getRole()).build();
				playerDetails.add(allPlayers);
			});
		});

		return playerDetails;
	}

	@Override
	public List<PlayerDto> getPlayerByLabelAndRole(String label, Role role) {
		List<List<Player>> playerList = list.stream().filter(e -> e.getLabel().equals(label)).map(Team::getPlayers)
				.collect(Collectors.toList());
		List<PlayerDto> playerInfo = new ArrayList<PlayerDto>();
		for (List<Player> list : playerList) {
			List<Player> details = list.stream().filter(e -> e.getRole().equals(role.getRoles()))
					.collect(Collectors.toList());
			System.out.println("Details of player" + details);
		}

		return playerInfo;
	}

	@Override
	public List<TeamDto> getTeamDetails() {
		if (list != null) {
			List<TeamDto> teamInfo = new ArrayList<TeamDto>();
			list.stream().forEach(e -> {
				TeamDto teamDetails = TeamDto.builder().home(e.getHome()).city(e.getCity()).city(e.getCoach())
						.label(e.getLabel()).players(e.getPlayers()).build();
				teamInfo.add(teamDetails);
			});
			return teamInfo;
		}

		return null;
	}

	@Override
	public List<AmountDto> getAmountSpentByTeam(String label) {
		List<AmountDto> amount = new ArrayList<>();
		List<List<Player>> allPlayerDetails = list.stream().filter(e -> e.getLabel().equals(label))
				.map(Team::getPlayers).collect(Collectors.toList());
		for (List<Player> list : allPlayerDetails) {
			double amountSpentbyTeam = list.stream().mapToDouble(e -> e.getPrice()).sum();
			amount.add(AmountDto.builder().price(amountSpentbyTeam).build());

		}
		return amount;

	}

	@Override
	public List<RoleAmountDto> getAmountSpentOnRoleByTeam(String label) {
		List<List<Player>> playerList = list.stream().filter(e -> e.getLabel().equals(label)).map(Team::getPlayers)
				.collect(Collectors.toList());
		List<RoleAmountDto> roleAmount = new ArrayList<>();
		for (List<Player> list1 : playerList) {
			double spentOnBowler = list1.stream().filter(r -> r.getRole().equals("Bowler"))
					.mapToDouble(p -> p.getPrice()).sum();
			double spentOnBatsMan = list1.stream().filter(r -> r.getRole().equals("Batsman"))
					.mapToDouble(p -> p.getPrice()).sum();
			double spentOnWicketKeeper = list1.stream().filter(r -> r.getRole().equals("Wicket Keeper"))
					.mapToDouble(p -> p.getPrice()).sum();
			double spentOnAllRounder = list1.stream().filter(r -> r.getRole().equals("All-Rounder"))
					.mapToDouble(p -> p.getPrice()).sum();
			roleAmount.add(RoleAmountDto.builder().role("All-Rounder").price(spentOnAllRounder).build());
			roleAmount.add(RoleAmountDto.builder().role("Bowler").price(spentOnBowler).build());
			roleAmount.add(RoleAmountDto.builder().role("Wicket Keeper").price(spentOnWicketKeeper).build());
			roleAmount.add(RoleAmountDto.builder().role("Batsman").price(spentOnBatsMan).build());
		}
		return roleAmount;

	}

	@Override
	public Map<Role, List<PlayerDto>> getMaxPaidPlayerForEachRole() {
		return null;
	}

}
