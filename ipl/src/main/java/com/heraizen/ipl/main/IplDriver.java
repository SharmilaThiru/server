package com.heraizen.ipl.main;

import java.util.List;
import java.util.Scanner;

import org.yaml.snakeyaml.Yaml;

import com.heraizen.ipl.domain.Role;
import com.heraizen.ipl.domain.Team;
import com.heraizen.ipl.service.IplService;
import com.heraizen.ipl.service.IplServiceImpl;
import com.heraizen.ipl.util.JsonReaderUtil;

public class IplDriver {

	public static void main(String[] args) {
		List<Team> teamList = JsonReaderUtil.loadDataFromJson();
		IplService service = IplServiceImpl.getInstance();
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Enter choice \n1.Get All TeamLabels 2.GetPlayers by label "
					+ "3.Role Count by label 4.AllPlayers 5.PlayerByRoleAndLabel 6.All team details 7.Amount Spent on role role by team"
					+ "Amount spent by team");

			int choice = sc.nextInt();
			switch (choice) {
			case 1:
				service.getTeamLabels().getLabels().forEach(System.out::println);

				break;
			case 2:
				service.getPlayersByLabel("MI").forEach(System.out::println);
				break;
			case 3:
				service.getRoleCountByLabel("CSK").forEach(System.out::println);
				break;
			case 4:
				service.getAllPlayers().forEach(System.out::println);
				break;
			case 5:
				Role role = Role.builder().roles("Batsman").build();
				service.getPlayerByLabelAndRole("CSK", role);
				break;
			case 6:
				service.getTeamDetails().forEach(System.out::println);
				break;
			case 7:
				service.getAmountSpentOnRoleByTeam("CSK").forEach(System.out::println);
				break;
			case 8:
				service.getAmountSpentByTeam("CSK").forEach(System.out::println);
				break;
			case 9:
				System.exit(0);
				break;
			default:
				System.out.println("Wrong options");
				break;
			}

		} while (true);
	}

}
